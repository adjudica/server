FROM python:3.7

WORKDIR /app
ENV PORT 8000
EXPOSE $PORT

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

ADD . /app

# Run tests during build so we avoid publishing images in bad shape
RUN python manage.py test -v 2
