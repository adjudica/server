import django
import pytest
from django.test import TestCase

from provider.models import Provider


class ProviderTestCase(TestCase):

    def test_create_provider(self):
        prv = Provider.objects.create(
            id=1,
            name="A Company"
        )

        assert prv.id == 1
        assert prv.name == 'A Company'
        assert str(prv) == 'A Company'

    def test_unique_id(self):
        Provider.objects.create(
            id=1,
            name='A Company'
        )

        with pytest.raises(django.db.utils.IntegrityError) as err:
            Provider.objects.create(
                id=1,
                name='Another Company'
            )

        assert str(err.value) == 'UNIQUE constraint failed: provider_provider.id'

    def test_duplicate_name(self):
        prv1 = Provider.objects.create(
            id=1,
            name='José Manuel Lopez Castillo'
        )

        prv2 = Provider.objects.create(
            id=999,
            name='José Manuel Lopez Castillo'
        )

        assert prv1.id != prv2.id
        assert prv1.name == prv2.name
        assert Provider.objects.filter(
            name='José Manuel Lopez Castillo'
        ).count() == 2
