from django.db import models


class Organization(models.Model):
    """An Organization
    """

    id = models.BigIntegerField(
        primary_key=True,
        db_index=True
    )
    name = models.CharField(
        'organization',
        max_length=100, 
        unique=True,
        db_index=True
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
