import django
import pytest
from django.test import TestCase

from org.models import Organization


class OrgTestCase(TestCase):

    def test_create(self):
        org = Organization.objects.create(
            id=1,
            name='Government Agency'
        )
        assert org.id == 1
        assert org.name == 'Government Agency'
        assert str(org) == 'Government Agency'

    def test_unique_id(self):
        Organization.objects.create(
            id=1,
            name='Government Agency'
        )

        with pytest.raises(django.db.utils.IntegrityError) as err:
            Organization.objects.create(
                id=1,
                name='Another Agency'
            )

        assert str(err.value) == 'UNIQUE constraint failed: org_organization.id'

    def test_unique_name(self):
        Organization.objects.create(
            id=1,
            name='Government Agency'
        )

        with pytest.raises(django.db.utils.IntegrityError) as err:
            Organization.objects.create(
                id=2,
                name='Government Agency'
            )

        assert str(err.value) == 'UNIQUE constraint failed: org_organization.name'
